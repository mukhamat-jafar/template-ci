<div class="modal fade" id="addModal">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
	<h4 class="modal-title">Add Data Karyawan</h4>
	<button type="button" class="close float-lg-right" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
	<form action="<?php echo base_url();?>list_karyawan/insert_data" method="POST">
	<div class="card-body">
		<div class="form-group">
		<label for="exampleInputEmail1">Id</label>
		<input type="text" class="form-control" name="id" id="exampleInputEmail1" placeholder="Enter Your Id">
		</div>
		<div class="form-group">
		<label for="exampleInputEmail1">Nama Karyawan</label>
		<input type="text" class="form-control" name="nama_karyawan" id="exampleInputEmail1" placeholder="Enter Your Nama">
		</div>
		<div class="form-group">
		<label for="exampleInputPassword1">Alamat</label>
		<input type="text" class="form-control" name="alamat" id="exampleInputPassword1" placeholder="Enter Your Alamat">
		</div>
		<div class="form-group">
		<label for="exampleInputPassword1">No HP</label>
		<input type="text" class="form-control" name="no_hp" id="exampleInputPassword1" placeholder="Enter Your No.HP">
		</div>
		<div class="form-group">
		<label for="exampleInputPassword1">Email</label>
		<input type="text" class="form-control" name="email" id="exampleInputPassword1" placeholder="Enter Your Email">
		</div>
		<div class="form-group">
		<label for="exampleInputPassword1">Dept</label>
		<input type="text" class="form-control" name="dept" id="exampleInputPassword1" placeholder="Enter Your Dept">
		</div>
		<div class="form-group">
		<label for="exampleInputPassword1">Jabatan</label>
		<input type="text" class="form-control" name="jabatan" id="exampleInputPassword1" placeholder="Enter Your Jabatan">
		</div>
	</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
<input type="submit" class="btn btn-success" value="save" />
</div>
</form>
</div>
</div>
</div>

<!-- Edit Data Modal -->
<div class="modal fade" id="EditModal">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
	<h4 class="modal-title">Edit Data Karyawan</h4>
	<button type="button" class="close float-lg-right" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
	<form action="<?php echo base_url();?>list_karyawan/update_data" method="POST">
	<div class="card-body">
		<div class="form-group">
		<label for="exampleInputEmail1">Id</label>
		<input type="text" class="form-control id" name="id" id="exampleInputEmail1" placeholder="Enter Your Id" readonly>
		</div>
		<div class="form-group">
		<label for="exampleInputEmail1">Nama Karyawan</label>
		<input type="text" class="form-control nama_karyawan" name="nama_karyawan" id="exampleInputEmail1" placeholder="Enter Your Nama">
		</div>
		<div class="form-group">
		<label for="exampleInputPassword1">Alamat</label>
		<input type="text" class="form-control alamat" name="alamat" id="exampleInputPassword1" placeholder="Enter Your Alamat">
		</div>
		<div class="form-group">
		<label for="exampleInputPassword1">No HP</label>
		<input type="text" class="form-control no_hp" name="no_hp" id="exampleInputPassword1" placeholder="Enter Your No.HP">
		</div>
		<div class="form-group">
		<label for="exampleInputPassword1">Email</label>
		<input type="text" class="form-control email" name="email" id="exampleInputPassword1" placeholder="Enter Your Email">
		</div>
		<div class="form-group">
		<label for="exampleInputPassword1">Dept</label>
		<input type="text" class="form-control dept" name="dept" id="exampleInputPassword1" placeholder="Enter Your Dept">
		</div>
		<div class="form-group">
		<label for="exampleInputPassword1">Jabatan</label>
		<input type="text" class="form-control jabatan" name="jabatan" id="exampleInputPassword1" placeholder="Enter Your Jabatan">
		</div>
	</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
<input type="submit" class="btn btn-success" value="save" />
</div>
</form>
</div>
</div>
</div>
