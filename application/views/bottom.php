<!-- jQuery -->
<script src="<?php echo base_url()?>/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url()?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url()?>/assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url()?>/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>/assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url()?>/assets/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
	});
	
	// Get Data karyawan
	$(".edit_data").click(function(){
		$(".id").val($(this).attr('data-id'));
		$(".nama_karyawan").val($(this).attr('data-nama_karyawan'));
		$(".alamat").val($(this).attr('data-alamat'));
		$(".no_hp").val($(this).attr('data-no_hp'));
		$(".email").val($(this).attr('data-email'));
		$(".dept").val($(this).attr('data-dept'));
		$(".jabatan").val($(this).attr('data-jabatan'));
	});
</script>
</body>
</html>
