<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('login')==TRUE)
		redirect('list_karyawan');
		else
		$this->load->view('login');
	}

	//action Login
	public function action_login(){
		if($this->input->method(TRUE) == "POST" && !empty($_POST)){
			$act['username'] = $this->input->post('username');
			$act['password'] = $this->input->post('password');
			$this->AuthModel->get_user($act);
		}
	}
}
